## DOC

this repo helps you to fill .env file while deploying to server, there is no magic just some str_replace, ask(); and fancy ascii logo printing 
<br>

## THINGS YOU NEED TO KNOW

running installation command;
<br>
<code>php artisan dd:install</code>
<br>
<br>
publishing command to change path of ascii logo via config file 
<br>
<code>php artisan vendor:publish dd/laravel_installation_helper-publish-config</code>
<br>
<br>
and this is how you reach to config; config(envInstallation.logoPath); 
<br>
<br>