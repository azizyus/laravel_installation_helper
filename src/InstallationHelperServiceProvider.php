<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 02.12.2018
 * Time: 22:53
 */


namespace LaravelInstallationHelper;
class InstallationHelperServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {


        $configPath = __DIR__."/configs/envInstallation.php";
        $this->mergeConfigFrom($configPath,"envInstallation");

        $this->publishes([

            $configPath => config_path("envInstallation.php")

        ],"dd/laravel_installation_helper-publish-config");

        $this->app->singleton("command.envInstallationHelper.install",function (){

            return new EnvInstallation();

        });


        $this->commands("command.envInstallationHelper.install");

    }



}