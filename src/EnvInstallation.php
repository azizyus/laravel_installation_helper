<?php

namespace LaravelInstallationHelper;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class EnvInstallation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dd:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'basic installation helper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $this->comment(file_get_contents(config("envInstallation.logoPath")));

        $envExampleFile = file_get_contents(base_path(".env.example"));
        $isLocal = $this->confirm("are you installing for local?");
        $isDevelopmentActivated = $this->confirm("dev mode on? [development/production]");


        $projectName = $this->ask("project name?");
        $domain = $this->ask("domain? (http://domain:port and dont use port for 80)");
        while (True)
        {

            $dbHost = $this->ask("Database host (probably localhost but if you are using docker it could be 'mysql')?");
            $dbName = $this->ask("Database name?");
            $dbUserName = $this->ask("Database username? (for local probably its root)");
            $dbPass = $this->ask("Database password?");

            try
            {

                Config::set('database.connections.testdb', array(
                    'driver'    => 'mysql',
                    'host'      => $dbHost,
                    'database'  => $dbName,
                    'username'  => $dbUserName,
                    'password'  => $dbPass,
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                ));


                DB::reconnect("testdb")->getPdo();
                break;
            }
            catch (\Exception $exception)
            {
                $this->comment("cant connect to db with credentials you have provided ");
            }


        }



        if(!$isLocal)
        {
            $mailServer = $this->ask("mail server name? (for cpanel its mail.domain.com)");
            $mailServerUserName = $this->ask("mail server username (generally its cpanel username)");
            $mailServerPass = $this->ask("mail server password (generally its cpanel password)");
        }


        if($isDevelopmentActivated) $envExampleFile = str_replace("APP_ENV=local","APP_ENV=development",$envExampleFile);
        else $envExampleFile = str_replace("APP_ENV=local","APP_ENV=production",$envExampleFile);

        $envExampleFile = str_replace("DB_HOST=127.0.0.1","DB_HOST=$dbHost",$envExampleFile);
        $envExampleFile = str_replace("DB_DATABASE=homestead","DB_DATABASE=$dbName",$envExampleFile);
        $envExampleFile = str_replace("DB_USERNAME=homestead","DB_USERNAME=$dbUserName",$envExampleFile);
        $envExampleFile = str_replace("DB_PASSWORD=secret","DB_PASSWORD=$dbPass",$envExampleFile);


        if(!$isLocal)
        {
            $envExampleFile = str_replace("MAIL_HOST=smtp.mailtrap.io","MAIL_HOST=$mailServer",$envExampleFile);
            $envExampleFile = str_replace("MAIL_USERNAME=null","MAIL_USERNAME=$mailServerUserName",$envExampleFile);
            $envExampleFile = str_replace("MAIL_PASSWORD=null","MAIL_PASSWORD=$mailServerPass",$envExampleFile);
        }





        $envExampleFile = str_replace("APP_NAME=Laravel","APP_NAME=$projectName",$envExampleFile);
        $envExampleFile = str_replace("APP_URL=http://localhost","APP_URL=$domain",$envExampleFile);

        $overrideEnv = $this->confirm("override .env?");

        if($overrideEnv)
        {
            file_put_contents(base_path(".env"),$envExampleFile);
        }
        else
        {
            file_put_contents(base_path(".env.installation"),$envExampleFile);
        }

    }
}
